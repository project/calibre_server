<?php

function calibre_server_define_books_view(){
	$view = new view();
	$view->name = 'calibre_libros';
	$view->description = 'Books list';
	$view->tag = 'calibre server';
	$view->base_table = 'books';
	$view->human_name = 'books';
	$view->core = 7;
	$view->api_version = '3.0';
	$view->disabled = FALSE;

	/* Display: Master */
	$handler = $view->new_display('default', 'Master', 'default');
	$handler->display->display_options['title'] = 'libros';
	$handler->display->display_options['use_more_always'] = FALSE;
	$handler->display->display_options['use_more_text'] = 'more';
	$handler->display->display_options['access']['type'] = 'none';
	$handler->display->display_options['cache']['type'] = 'none';
	$handler->display->display_options['query']['type'] = 'views_query';
	$handler->display->display_options['exposed_form']['type'] = 'basic';
	$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Apply';
	$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reset';
	$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Sort by';
	$handler->display->display_options['pager']['type'] = 'full';
	$handler->display->display_options['pager']['options']['items_per_page'] = '20';
	$handler->display->display_options['pager']['options']['offset'] = '0';
	$handler->display->display_options['pager']['options']['id'] = '0';
	$handler->display->display_options['pager']['options']['quantity'] = '9';
	$handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Items per page';
	$handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- All -';
	$handler->display->display_options['pager']['options']['expose']['offset_label'] = 'Offset';
	$handler->display->display_options['pager']['options']['tags']['first'] = '« first';
	$handler->display->display_options['pager']['options']['tags']['previous'] = '‹ previous';
	$handler->display->display_options['pager']['options']['tags']['next'] = 'next ›';
	$handler->display->display_options['pager']['options']['tags']['last'] = 'last »';
	$handler->display->display_options['style_plugin'] = 'default';
	$handler->display->display_options['row_plugin'] = 'fields';
	/* Campo: Contenido: Book Id */
	$handler->display->display_options['fields']['id']['id'] = 'id';
	$handler->display->display_options['fields']['id']['table'] = 'books';
	$handler->display->display_options['fields']['id']['field'] = 'id';
	$handler->display->display_options['fields']['id']['label'] = '';
	$handler->display->display_options['fields']['id']['exclude'] = TRUE;
	$handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
	/* Campo: Contenido: Título */
	$handler->display->display_options['fields']['title']['id'] = 'title';
	$handler->display->display_options['fields']['title']['table'] = 'books';
	$handler->display->display_options['fields']['title']['field'] = 'title';
	$handler->display->display_options['fields']['title']['label'] = '';
	$handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
	$handler->display->display_options['fields']['title']['alter']['text'] = '[title]';
	$handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
	$handler->display->display_options['fields']['title']['alter']['path'] = 'books-view/[id]';
	$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
	/* Criterio de ordenación: Contenido: Título */
	$handler->display->display_options['sorts']['title']['id'] = 'title';
	$handler->display->display_options['sorts']['title']['table'] = 'books';
	$handler->display->display_options['sorts']['title']['field'] = 'title';
	/* Filtro contextual: Contenido: Author Id */
	$handler->display->display_options['arguments']['id']['id'] = 'id';
	$handler->display->display_options['arguments']['id']['table'] = 'authors';
	$handler->display->display_options['arguments']['id']['field'] = 'id';
	$handler->display->display_options['arguments']['id']['exception']['title'] = 'All(s)';
	$handler->display->display_options['arguments']['id']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['id']['summary']['format'] = 'default_summary';
	/* Filtro contextual: Global: Nulo */
	$handler->display->display_options['arguments']['null']['id'] = 'null';
	$handler->display->display_options['arguments']['null']['table'] = 'views';
	$handler->display->display_options['arguments']['null']['field'] = 'null';
	$handler->display->display_options['arguments']['null']['exception']['title'] = 'All(s)';
	$handler->display->display_options['arguments']['null']['title_enable'] = TRUE;
	$handler->display->display_options['arguments']['null']['title'] = '%2';
	$handler->display->display_options['arguments']['null']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['null']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['null']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['null']['summary_options']['items_per_page'] = '25';
	/* Criterios de filtrado: Contenido: Título */
	$handler->display->display_options['filters']['title']['id'] = 'title';
	$handler->display->display_options['filters']['title']['table'] = 'books';
	$handler->display->display_options['filters']['title']['field'] = 'title';
	$handler->display->display_options['filters']['title']['operator'] = 'contains';
	$handler->display->display_options['filters']['title']['exposed'] = TRUE;
	$handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
	$handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
	$handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
	$handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
	$handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
		2 => '2',
		1 => 0,
		5 => 0,
		4 => 0,
		3 => 0,
	);

	/* Display: Page */
	$handler = $view->new_display('page', 'Page', 'page');
	$handler->display->display_options['defaults']['title'] = FALSE;
	$handler->display->display_options['title'] = 'Books';
	$handler->display->display_options['defaults']['style_plugin'] = FALSE;
	$handler->display->display_options['style_plugin'] = 'table';
	$handler->display->display_options['style_options']['columns'] = array(
		'id' => 'id',
		'title' => 'title',
	);
	$handler->display->display_options['style_options']['default'] = '-1';
	$handler->display->display_options['style_options']['info'] = array(
		'id' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		),
		'title' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		),
	);
	$handler->display->display_options['defaults']['style_options'] = FALSE;
	$handler->display->display_options['defaults']['row_plugin'] = FALSE;
	$handler->display->display_options['defaults']['row_options'] = FALSE;
	$handler->display->display_options['defaults']['arguments'] = FALSE;
	/* Filtro contextual: Contenido: Título */
	$handler->display->display_options['arguments']['title']['id'] = 'title';
	$handler->display->display_options['arguments']['title']['table'] = 'books';
	$handler->display->display_options['arguments']['title']['field'] = 'title';
	$handler->display->display_options['arguments']['title']['default_action'] = 'default';
	$handler->display->display_options['arguments']['title']['exception']['title_enable'] = TRUE;
	$handler->display->display_options['arguments']['title']['exception']['title'] = 'All(s)';
	$handler->display->display_options['arguments']['title']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['title']['default_argument_options']['argument'] = 'A';
	$handler->display->display_options['arguments']['title']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['title']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['title']['summary_options']['items_per_page'] = '25';
	$handler->display->display_options['arguments']['title']['specify_validation'] = TRUE;
	$handler->display->display_options['arguments']['title']['glossary'] = TRUE;
	$handler->display->display_options['arguments']['title']['limit'] = '1';
	$handler->display->display_options['defaults']['filter_groups'] = FALSE;
	$handler->display->display_options['defaults']['filters'] = FALSE;
	$handler->display->display_options['path'] = 'calibre-books';
	$handler->display->display_options['menu']['type'] = 'normal';
	$handler->display->display_options['menu']['title'] = 'Books';
	$handler->display->display_options['menu']['description'] = 'Calibre books';
	$handler->display->display_options['menu']['weight'] = '1';
	$handler->display->display_options['menu']['name'] = 'calibre-library';
	$handler->display->display_options['menu']['context'] = 0;
	$handler->display->display_options['menu']['context_only_inline'] = 0;

	/* Display: Lista por autor */
	$handler = $view->new_display('page', 'Lista por autor', 'page_1');
	$handler->display->display_options['defaults']['style_plugin'] = FALSE;
	$handler->display->display_options['style_plugin'] = 'table';
	$handler->display->display_options['style_options']['columns'] = array(
		'id' => 'id',
		'title' => 'title',
	);
	$handler->display->display_options['style_options']['default'] = '-1';
	$handler->display->display_options['style_options']['info'] = array(
		'id' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		),
		'title' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		),
	);
	$handler->display->display_options['defaults']['style_options'] = FALSE;
	$handler->display->display_options['defaults']['row_plugin'] = FALSE;
	$handler->display->display_options['defaults']['row_options'] = FALSE;
	$handler->display->display_options['defaults']['arguments'] = FALSE;
	/* Filtro contextual: Contenido: Author Id */
	$handler->display->display_options['arguments']['id']['id'] = 'id';
	$handler->display->display_options['arguments']['id']['table'] = 'authors';
	$handler->display->display_options['arguments']['id']['field'] = 'id';
	$handler->display->display_options['arguments']['id']['default_action'] = 'empty';
	$handler->display->display_options['arguments']['id']['exception']['title'] = 'All(s)';
	$handler->display->display_options['arguments']['id']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['id']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['id']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['id']['summary_options']['items_per_page'] = '25';
	/* Filtro contextual: Global: Nulo */
	$handler->display->display_options['arguments']['null']['id'] = 'null';
	$handler->display->display_options['arguments']['null']['table'] = 'views';
	$handler->display->display_options['arguments']['null']['field'] = 'null';
	$handler->display->display_options['arguments']['null']['exception']['title'] = 'All(s)';
	$handler->display->display_options['arguments']['null']['title_enable'] = TRUE;
	$handler->display->display_options['arguments']['null']['title'] = 'Books - Author: %2';
	$handler->display->display_options['arguments']['null']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['null']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['null']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['null']['summary_options']['items_per_page'] = '25';
	$handler->display->display_options['defaults']['filter_groups'] = FALSE;
	$handler->display->display_options['defaults']['filters'] = FALSE;
	$handler->display->display_options['path'] = 'books-author-list';

	/* Display: Lista por editorial */
	$handler = $view->new_display('page', 'Lista por editorial', 'page_2');
	$handler->display->display_options['defaults']['style_plugin'] = FALSE;
	$handler->display->display_options['style_plugin'] = 'table';
	$handler->display->display_options['style_options']['columns'] = array(
		'id' => 'id',
		'title' => 'title',
	);
	$handler->display->display_options['style_options']['default'] = '-1';
	$handler->display->display_options['style_options']['info'] = array(
		'id' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		),
		'title' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		),
	);
	$handler->display->display_options['defaults']['style_options'] = FALSE;
	$handler->display->display_options['defaults']['row_plugin'] = FALSE;
	$handler->display->display_options['defaults']['row_options'] = FALSE;
	$handler->display->display_options['defaults']['arguments'] = FALSE;
	/* Filtro contextual: Contenido: Publisher Id */
	$handler->display->display_options['arguments']['id']['id'] = 'id';
	$handler->display->display_options['arguments']['id']['table'] = 'publishers';
	$handler->display->display_options['arguments']['id']['field'] = 'id';
	$handler->display->display_options['arguments']['id']['default_action'] = 'empty';
	$handler->display->display_options['arguments']['id']['exception']['title'] = 'All(s)';
	$handler->display->display_options['arguments']['id']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['id']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['id']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['id']['summary_options']['items_per_page'] = '25';
	/* Filtro contextual: Global: Nulo */
	$handler->display->display_options['arguments']['null']['id'] = 'null';
	$handler->display->display_options['arguments']['null']['table'] = 'views';
	$handler->display->display_options['arguments']['null']['field'] = 'null';
	$handler->display->display_options['arguments']['null']['exception']['title'] = 'All(s)';
	$handler->display->display_options['arguments']['null']['title_enable'] = TRUE;
	$handler->display->display_options['arguments']['null']['title'] = 'Books - Publisher: %2';
	$handler->display->display_options['arguments']['null']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['null']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['null']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['null']['summary_options']['items_per_page'] = '25';
	$handler->display->display_options['defaults']['filter_groups'] = FALSE;
	$handler->display->display_options['defaults']['filters'] = FALSE;
	$handler->display->display_options['path'] = 'books-publisher-list';

	/* Display: Lista por serie */
	$handler = $view->new_display('page', 'Lista por serie', 'page_3');
	$handler->display->display_options['defaults']['style_plugin'] = FALSE;
	$handler->display->display_options['style_plugin'] = 'table';
	$handler->display->display_options['style_options']['columns'] = array(
		'id' => 'id',
		'title' => 'title',
	);
	$handler->display->display_options['style_options']['default'] = '-1';
	$handler->display->display_options['style_options']['info'] = array(
		'id' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		),
		'title' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		),
	);
	$handler->display->display_options['defaults']['style_options'] = FALSE;
	$handler->display->display_options['defaults']['row_plugin'] = FALSE;
	$handler->display->display_options['defaults']['row_options'] = FALSE;
	$handler->display->display_options['defaults']['arguments'] = FALSE;
	/* Filtro contextual: Contenido: Serie Id */
	$handler->display->display_options['arguments']['id']['id'] = 'id';
	$handler->display->display_options['arguments']['id']['table'] = 'series';
	$handler->display->display_options['arguments']['id']['field'] = 'id';
	$handler->display->display_options['arguments']['id']['default_action'] = 'empty';
	$handler->display->display_options['arguments']['id']['exception']['title'] = 'All(s)';
	$handler->display->display_options['arguments']['id']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['id']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['id']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['id']['summary_options']['items_per_page'] = '25';
	/* Filtro contextual: Global: Nulo */
	$handler->display->display_options['arguments']['null']['id'] = 'null';
	$handler->display->display_options['arguments']['null']['table'] = 'views';
	$handler->display->display_options['arguments']['null']['field'] = 'null';
	$handler->display->display_options['arguments']['null']['exception']['title'] = 'All(s)';
	$handler->display->display_options['arguments']['null']['title_enable'] = TRUE;
	$handler->display->display_options['arguments']['null']['title'] = 'Books - Serie: %2';
	$handler->display->display_options['arguments']['null']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['null']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['null']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['null']['summary_options']['items_per_page'] = '25';
	$handler->display->display_options['defaults']['filter_groups'] = FALSE;
	$handler->display->display_options['defaults']['filters'] = FALSE;
	$handler->display->display_options['path'] = 'books-serie-list';

	/* Display: Lista por etiqueta */
	$handler = $view->new_display('page', 'Lista por etiqueta', 'page_4');
	$handler->display->display_options['defaults']['style_plugin'] = FALSE;
	$handler->display->display_options['style_plugin'] = 'table';
	$handler->display->display_options['style_options']['columns'] = array(
		'id' => 'id',
		'title' => 'title',
	);
	$handler->display->display_options['style_options']['default'] = '-1';
	$handler->display->display_options['style_options']['info'] = array(
		'id' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		),
		'title' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		),
	);
	$handler->display->display_options['defaults']['style_options'] = FALSE;
	$handler->display->display_options['defaults']['row_plugin'] = FALSE;
	$handler->display->display_options['defaults']['row_options'] = FALSE;
	$handler->display->display_options['defaults']['arguments'] = FALSE;
	/* Filtro contextual: Contenido: Tag Id */
	$handler->display->display_options['arguments']['id']['id'] = 'id';
	$handler->display->display_options['arguments']['id']['table'] = 'tags';
	$handler->display->display_options['arguments']['id']['field'] = 'id';
	$handler->display->display_options['arguments']['id']['default_action'] = 'empty';
	$handler->display->display_options['arguments']['id']['exception']['title'] = 'All(s)';
	$handler->display->display_options['arguments']['id']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['id']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['id']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['id']['summary_options']['items_per_page'] = '25';
	/* Filtro contextual: Global: Nulo */
	$handler->display->display_options['arguments']['null']['id'] = 'null';
	$handler->display->display_options['arguments']['null']['table'] = 'views';
	$handler->display->display_options['arguments']['null']['field'] = 'null';
	$handler->display->display_options['arguments']['null']['exception']['title'] = 'All(s)';
	$handler->display->display_options['arguments']['null']['title_enable'] = TRUE;
	$handler->display->display_options['arguments']['null']['title'] = 'Books - Tag: %2';
	$handler->display->display_options['arguments']['null']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['null']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['null']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['null']['summary_options']['items_per_page'] = '25';
	$handler->display->display_options['defaults']['filter_groups'] = FALSE;
	$handler->display->display_options['defaults']['filters'] = FALSE;
	$handler->display->display_options['path'] = 'books-tag-list';
	$handler->display->display_options['menu']['title'] = 'Tags';
	$handler->display->display_options['menu']['description'] = 'List of tags';
	$handler->display->display_options['menu']['weight'] = '0';
	$handler->display->display_options['menu']['name'] = 'calibre-library';
	$handler->display->display_options['menu']['context'] = 0;
	$handler->display->display_options['menu']['context_only_inline'] = 0;

	/* Display: Attachment */
	$handler = $view->new_display('attachment', 'Attachment', 'attachment_1');
	$handler->display->display_options['defaults']['title'] = FALSE;
	$handler->display->display_options['pager']['type'] = 'none';
	$handler->display->display_options['pager']['options']['offset'] = '0';
	$handler->display->display_options['defaults']['arguments'] = FALSE;
	/* Filtro contextual: Contenido: Título */
	$handler->display->display_options['arguments']['title']['id'] = 'title';
	$handler->display->display_options['arguments']['title']['table'] = 'books';
	$handler->display->display_options['arguments']['title']['field'] = 'title';
	$handler->display->display_options['arguments']['title']['default_action'] = 'summary';
	$handler->display->display_options['arguments']['title']['exception']['title_enable'] = TRUE;
	$handler->display->display_options['arguments']['title']['exception']['title'] = 'All(s)';
	$handler->display->display_options['arguments']['title']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['title']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['title']['summary']['format'] = 'unformatted_summary';
	$handler->display->display_options['arguments']['title']['summary_options']['count'] = FALSE;
	$handler->display->display_options['arguments']['title']['summary_options']['items_per_page'] = '25';
	$handler->display->display_options['arguments']['title']['summary_options']['inline'] = TRUE;
	$handler->display->display_options['arguments']['title']['specify_validation'] = TRUE;
	$handler->display->display_options['arguments']['title']['glossary'] = TRUE;
	$handler->display->display_options['arguments']['title']['limit'] = '1';
	$handler->display->display_options['displays'] = array(
		'page' => 'page',
		'default' => 0,
		'page_1' => 0,
		'page_2' => 0,
		'page_3' => 0,
		'page_4' => 0,
	);
	$handler->display->display_options['inherit_arguments'] = FALSE;
	$translatables['libros'] = array(
		t('Master'),
		t('books'),
		t('more'),
		t('Apply'),
		t('Reset'),
		t('Sort by'),
		t('Asc'),
		t('Desc'),
		t('Items per page'),
		t('- All -'),
		t('Offset'),
		t('« first'),
		t('‹ previous'),
		t('next ›'),
		t('last »'),
		t('.'),
		t(','),
		t('[title]'),
		t('All(s)'),
		t('%2'),
		t('Title'),
		t('Page'),
		t('Books'),
		t('List by author'),
		t('Books - Author: %2'),
		t('List by publisher'),
		t('Books - Publisher: %2'),
		t('List by serie'),
		t('Books - Serie: %2'),
		t('List by tag'),
		t('Books - Tag: %2'),
		t('Attachment'),
		t('Books list'),
	);
	return $view;
}
