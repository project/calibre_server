<?php

function calibre_server_define_series_view(){
	$view = new view();
	$view->name = 'calibre_series';
	$view->description = 'Series list';
	$view->tag = 'calibre server';
	$view->base_table = 'series';
	$view->human_name = 'Series';
	$view->core = 7;
	$view->api_version = '3.0';
	$view->disabled = FALSE;

	/* Display: Master */
	$handler = $view->new_display('default', 'Master', 'default');
	$handler->display->display_options['title'] = 'Series';
	$handler->display->display_options['use_more_always'] = FALSE;
	$handler->display->display_options['use_more_text'] = 'more';
	$handler->display->display_options['access']['type'] = 'none';
	$handler->display->display_options['cache']['type'] = 'none';
	$handler->display->display_options['query']['type'] = 'views_query';
	$handler->display->display_options['exposed_form']['type'] = 'basic';
	$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Apply';
	$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reset';
	$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Sort by';
	$handler->display->display_options['pager']['type'] = 'full';
	$handler->display->display_options['pager']['options']['items_per_page'] = '20';
	$handler->display->display_options['pager']['options']['offset'] = '0';
	$handler->display->display_options['pager']['options']['id'] = '0';
	$handler->display->display_options['pager']['options']['quantity'] = '9';
	$handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Items per page';
	$handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- All -';
	$handler->display->display_options['pager']['options']['expose']['offset_label'] = 'Offset';
	$handler->display->display_options['pager']['options']['tags']['first'] = '« first';
	$handler->display->display_options['pager']['options']['tags']['previous'] = '‹ previous';
	$handler->display->display_options['pager']['options']['tags']['next'] = 'next ›';
	$handler->display->display_options['pager']['options']['tags']['last'] = 'last »';
	$handler->display->display_options['style_plugin'] = 'table';
	$handler->display->display_options['style_options']['columns'] = array(
		'id' => 'id',
		'name' => 'name',
	);
	$handler->display->display_options['style_options']['default'] = '-1';
	$handler->display->display_options['style_options']['info'] = array(
		'id' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		),
		'name' => array(
			'sortable' => 0,
			'default_sort_order' => 'asc',
			'align' => '',
			'separator' => '',
			'empty_column' => 0,
		),
	);
	/* Campo: Contenido: Serie Id */
	$handler->display->display_options['fields']['id']['id'] = 'id';
	$handler->display->display_options['fields']['id']['table'] = 'series';
	$handler->display->display_options['fields']['id']['field'] = 'id';
	$handler->display->display_options['fields']['id']['label'] = '';
	$handler->display->display_options['fields']['id']['exclude'] = TRUE;
	$handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['id']['separator'] = '';
	/* Campo: Contenido: Series */
	$handler->display->display_options['fields']['name']['id'] = 'name';
	$handler->display->display_options['fields']['name']['table'] = 'series';
	$handler->display->display_options['fields']['name']['field'] = 'name';
	$handler->display->display_options['fields']['name']['label'] = '';
	$handler->display->display_options['fields']['name']['alter']['alter_text'] = TRUE;
	$handler->display->display_options['fields']['name']['alter']['text'] = '[name]';
	$handler->display->display_options['fields']['name']['alter']['make_link'] = TRUE;
	$handler->display->display_options['fields']['name']['alter']['path'] = 'books-serie-list/[id]/[name]';
	$handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;

	/* Display: Page */
	$handler = $view->new_display('page', 'Page', 'page');
	$handler->display->display_options['defaults']['arguments'] = FALSE;
	/* Filtro contextual: Contenido: Series */
	$handler->display->display_options['arguments']['name']['id'] = 'name';
	$handler->display->display_options['arguments']['name']['table'] = 'series';
	$handler->display->display_options['arguments']['name']['field'] = 'name';
	$handler->display->display_options['arguments']['name']['default_action'] = 'default';
	$handler->display->display_options['arguments']['name']['exception']['title_enable'] = TRUE;
	$handler->display->display_options['arguments']['name']['exception']['title'] = 'All(s)';
	$handler->display->display_options['arguments']['name']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['name']['default_argument_options']['argument'] = 'A';
	$handler->display->display_options['arguments']['name']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['name']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['name']['summary_options']['items_per_page'] = '25';
	$handler->display->display_options['arguments']['name']['specify_validation'] = TRUE;
	$handler->display->display_options['arguments']['name']['glossary'] = TRUE;
	$handler->display->display_options['arguments']['name']['limit'] = '1';
	$handler->display->display_options['path'] = 'series';
	$handler->display->display_options['menu']['type'] = 'normal';
	$handler->display->display_options['menu']['title'] = 'Series';
	$handler->display->display_options['menu']['description'] = 'List of series';
	$handler->display->display_options['menu']['weight'] = '5';
	$handler->display->display_options['menu']['name'] = 'calibre-library';
	$handler->display->display_options['menu']['context'] = 0;
	$handler->display->display_options['menu']['context_only_inline'] = 0;

	/* Display: Attachment */
	$handler = $view->new_display('attachment', 'Attachment', 'attachment_1');
	$handler->display->display_options['pager']['type'] = 'none';
	$handler->display->display_options['pager']['options']['offset'] = '0';
	$handler->display->display_options['defaults']['arguments'] = FALSE;
	/* Filtro contextual: Contenido: Series */
	$handler->display->display_options['arguments']['name']['id'] = 'name';
	$handler->display->display_options['arguments']['name']['table'] = 'series';
	$handler->display->display_options['arguments']['name']['field'] = 'name';
	$handler->display->display_options['arguments']['name']['default_action'] = 'summary';
	$handler->display->display_options['arguments']['name']['exception']['title_enable'] = TRUE;
	$handler->display->display_options['arguments']['name']['exception']['title'] = 'All(s)';
	$handler->display->display_options['arguments']['name']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['name']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['name']['summary']['format'] = 'unformatted_summary';
	$handler->display->display_options['arguments']['name']['summary_options']['count'] = FALSE;
	$handler->display->display_options['arguments']['name']['summary_options']['items_per_page'] = '25';
	$handler->display->display_options['arguments']['name']['summary_options']['inline'] = TRUE;
	$handler->display->display_options['arguments']['name']['glossary'] = TRUE;
	$handler->display->display_options['arguments']['name']['limit'] = '1';
	$handler->display->display_options['displays'] = array(
		'page' => 'page',
		'default' => 0,
	);
	$handler->display->display_options['inherit_arguments'] = FALSE;
	$translatables['series'] = array(
		t('Master'),
		t('Series'),
		t('more'),
		t('Apply'),
		t('Reset'),
		t('Sort by'),
		t('Asc'),
		t('Desc'),
		t('Items per page'),
		t('- All -'),
		t('Offset'),
		t('« first'),
		t('‹ previous'),
		t('next ›'),
		t('last »'),
		t('.'),
		t('[name]'),
		t('Page'),
		t('All(s)'),
		t('Attachment'),
		t('Series list'),
	);
	return $view;
}
